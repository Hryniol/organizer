Organizer to aplikacja do zarządzania projektami, dzięki której w łatwy sposób można sporządzić harmonogram prac oraz śledzić postępy. Aplikacja jest wzorowana na oprogramowaniu Jira.
W aplikacji zastosowano m.in. Javę, Springa, Hibernate'a, SQL, Thymeleaf'a oraz JavaScript.
Organizer posiada funckjonalności:
- rejestracji i logowania użytkownika
- dodawania/modyfikacji projektów  oraz sprintów
- delegowania zadań, ustawiania priorytetów zadań
- zmianę postępów zadań

Sprawdź : organizer.khryniewicki.com.pl

______________________________________________________________________________________________________________________________________________________________________________

Organizer is a project management application that allows you to easily create a work schedule and track progress. The application is based on the Jira software.
The application uses, among others Java, Spring, Hibernate, SQL, Thymeleaf and JavaScript.
Organizer has many functionalities, such as:
- user registration and login
- adding / modifying projects and sprints
- delegating tasks, setting task priorities
- changing task progress

Check: organizer.khryniewicki.com.pl